import 'reset-css'
import 'animate.css'

import React, { useState } from 'react'
import ReactDOM from 'react-dom'

import { AppState } from './types'

import WorkTop from './components/WorkTop'
import Welcome from './components/Welcome'
import Authorization from './components/Authorization'

export interface AppNavigationProps {
  setStage: (stage: AppState) => void
}

const App = () => {
  const [stage, setStage] = useState<AppState>(AppState.worktop)

  switch (stage) {
    case AppState.worktop:
      return <WorkTop setStage={setStage} />

    case AppState.welcome:
      return <Welcome setStage={setStage} />

    default:
      return <Authorization setStage={setStage} />
  }
}

ReactDOM.render(<App />, document.getElementById('root'))
