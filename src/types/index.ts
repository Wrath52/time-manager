export enum TimerState {
  playing,
  stopped,
  suspended,
}

export enum AppState {
  welcome,
  worktop,
  authorization,
}

export enum WelcomeState {
  hello,
  offer,
}
