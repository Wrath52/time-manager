import React, { useCallback, useEffect, useMemo, useState } from 'react'

import { Props } from './WelcomeTypes'
import { AppState, WelcomeState } from '../../types'

import './WelcomeStyle.scss'

const delay = 2000

const Welcome = (props: Props) => {
  const [stage, setStage] = useState<WelcomeState>(WelcomeState.hello)

  useEffect(() => {
    if (stage === WelcomeState.hello) {
      setTimeout(() => {
        setStage(WelcomeState.offer)
      }, delay)
    }
  }, [stage])

  const onStart = useCallback(() => {
    props.setStage(AppState.worktop)
  }, [props])

  const Hello = useMemo(() => {
    const animation =
      stage === WelcomeState.hello ? 'animate__fadeInUp' : 'animate__fadeOutUp'

    return (
      <h1
        children="Hello, friend!"
        className={`animate__animated animate__fadeInUp animate__delay-1s ${animation}`}
      />
    )
  }, [stage])

  const Offer = useMemo(() => {
    if (stage !== WelcomeState.offer) return null

    return (
      <div>
        <h1
          children="Working?"
          className={'animate__animated animate__fadeInUp animate__delay-2s'}
        />
        <button
          onClick={onStart}
          children="Let's start"
          className={'animate__animated animate__fadeInUp animate__delay-3s'}
        />
      </div>
    )
  }, [stage, onStart])

  return (
    <div className="Welcome">
      {Hello}
      {Offer}
    </div>
  )
}

export default Welcome
