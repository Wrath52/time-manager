import React, { useCallback, useEffect, useMemo, useState } from 'react'

import { Props } from './TimerTypes'
import { TimerState } from '../../types'

import './TimerStyle.scss'

const delay = 1000

const Timer = (props: Props) => {
  const [time, setTime] = useState<number>(0)

  useEffect(() => {
    switch (props.state) {
      case TimerState.playing:
        setTimeout(() => {
          setTime(time + delay)
        }, delay)
        break

      case TimerState.stopped:
        if (time !== 0) setTime(0)
        break

      default:
        break
    }
  }, [props.state, time])

  const formatTime = useCallback((time: number, isMilliseconds?: boolean) => {
    if (isMilliseconds) {
      if (time < 10) return '000'
      if (time < 100) return `0${time}`
    } else {
      if (time < 10) return `0${time}`
    }

    return time
  }, [])

  const timeObj = useMemo(() => {
    let seconds: string | number = Math.floor((time / 1000) % 60)
    let hours: string | number = Math.floor(time / 1000 / 60 / 60)
    let minutes: string | number = Math.floor((time / 1000 / 60) % 60)

    return {
      hours: formatTime(hours),
      minutes: formatTime(minutes),
      seconds: formatTime(seconds),
    }
  }, [time, formatTime])

  return (
    <div className="Timer">
      <div className="Timer__counters">
        <div className="Timer__counters-item">
          <div className="Timer__counters-item-value">{timeObj.hours}</div>
          <div className="Timer__counters-item-label">hh</div>
        </div>
        <div className="Timer__counters-item">
          <div className="Timer__counters-item-value">{timeObj.minutes}</div>
          <div className="Timer__counters-item-label">mm</div>
        </div>
        <div className="Timer__counters-item">
          <div className="Timer__counters-item-value">{timeObj.seconds}</div>
          <div className="Timer__counters-item-label">ss</div>
        </div>
      </div>
    </div>
  )
}

export default Timer
