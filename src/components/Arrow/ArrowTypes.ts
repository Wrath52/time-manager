export interface ArrowProps {
  onClick: () => void
  isNext: boolean
}
