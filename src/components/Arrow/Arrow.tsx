import React from 'react'

import { ArrowProps } from './ArrowTypes'
import './ArrowStyle.scss'

const Arrow = (props: ArrowProps) => {
  return (
    <div className="next" onClick={props.onClick}>
      {props.isNext ? '=>' : '<='}
    </div>
  )
}

export default Arrow
