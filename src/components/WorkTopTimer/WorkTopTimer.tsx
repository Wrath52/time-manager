import React, { useCallback, useState } from 'react'

import { TimerState } from '../../types'
import { WorkTopTimerProps } from './WorkTopTimerTypes'

import Timer from '../Timer'

const WorkTopTimer = (props: WorkTopTimerProps) => {
  const [state, setState] = useState(TimerState.stopped)

  const onStart = useCallback(() => {
    setState(TimerState.playing)
  }, [])

  const onStop = useCallback(() => {
    setState(TimerState.stopped)
  }, [])

  const onPause = useCallback(() => {
    setState(TimerState.suspended)
  }, [])

  return (
    <React.Fragment>
      <Timer state={state} />
      <div className="Timer__buttons">
        <button onClick={onStart}>start</button>
        <button onClick={onStop}>stop</button>
        <button onClick={onPause}>pause</button>
      </div>
    </React.Fragment>
  )
}

export default WorkTopTimer
