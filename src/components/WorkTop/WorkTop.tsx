import React, { useCallback, useEffect, useState } from 'react'
import WorkTopTimer from '../WorkTopTimer'

import { Props } from './WorkTopTypes'

import './WorkTopStyle.scss'

function useForceUpdate() {
  const [value, setValue] = useState(0)
  return () => setValue(value => ++value)
}

const WorkTop = (props: Props) => {
  const [slide, setSlide] = useState(1)
  const forceUpdate = useForceUpdate()

  useEffect(() => {
    window.addEventListener('resize', forceUpdate)
    return () => {
      window.removeEventListener('resize', forceUpdate)
    }
  }, [])

  const toPrevSlide = useCallback(() => {
    let newSlide = slide - 1
    if (newSlide < 0) newSlide = 2

    setSlide(newSlide)
  }, [slide])

  const toNextSlide = useCallback(() => {
    let newSlide = slide + 1
    if (newSlide > 2) newSlide = 0

    setSlide(newSlide)
  }, [slide])

  const width = document.body.clientWidth
  const transform = `translateX(-${slide * width}px)`

  return (
    <div className="WorkTop">
      <div className="arrows">
        <div className="prev" onClick={toPrevSlide}>
          {'<='}
        </div>
        <div className="next" onClick={toNextSlide}>
          {'=>'}
        </div>
      </div>
      <div className="WorkTop-slider" style={{ transform }}>
        <div style={{ minWidth: width, width, flex: 1, display: 'flex' }}>
          <div className="red"></div>
        </div>
        <div style={{ minWidth: width, width, flex: 1, display: 'flex' }}>
          <WorkTopTimer />
        </div>
        <div style={{ minWidth: width, width, flex: 1, display: 'flex' }}>
          <div className="red"></div>
        </div>
      </div>
    </div>
  )
}

export default WorkTop
